import React, { Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Quotes from "./components/pages/Quotes";
import Layout from "./components/layout/Layout";
import LoadingSpinner from "./components/UI/LoadingSpinner";

const NewQuote = React.lazy(() => import("./components/pages/NewQuote"));
const NoQuotesFound = React.lazy(() =>
  import("./components/quotes/NoQuotesFound")
);
const QuoteDetails = React.lazy(() =>
  import("./components/pages/QuoteDetails")
);

function App() {
  return (
    <Layout>
      <Suspense
        fallback={
          <div className="centered">
            <LoadingSpinner />
          </div>
        }
      >
        <Switch>
          <Route path="/" exact>
            <Redirect to="/quotes" />
          </Route>

          <Route path="/quotes" exact>
            <Quotes />
          </Route>

          <Route path="/quotes/:quoteId">
            <QuoteDetails />
          </Route>

          <Route path="/new-quote">
            <NewQuote />
          </Route>

          <Route path="*">
            <NoQuotesFound />
          </Route>
        </Switch>
      </Suspense>
    </Layout>
  );
}

export default App;
