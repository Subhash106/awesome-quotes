import { useState, useEffect } from "react";

import classes from "./Comments.module.css";
import NewCommentForm from "./NewCommentForm";
import CommentsList from "./CommentsList";
import useHttp from "../../hooks/use-http";
import { getAllComments } from "../../lib/app";
import { useParams } from "react-router-dom";

const Comments = () => {
  const [isAddingComment, setIsAddingComment] = useState(false);
  const { sendRequest, data: loadedComments, status, error } = useHttp(
    getAllComments,
    true
  );

  const params = useParams();

  const { quoteId } = params;

  const startAddCommentHandler = () => {
    setIsAddingComment(true);
  };

  useEffect(() => {
    sendRequest(quoteId);
  }, [quoteId, sendRequest]);

  const commentAddedHandler = () => {
    setIsAddingComment(false);
    sendRequest(quoteId);
  }

  if (error) {
    return <div className="centered focused">{error}</div>;
  }

  return (
    <section className={classes.comments}>
      <h2>User Comments</h2>
      {!isAddingComment && (
        <button className="btn" onClick={startAddCommentHandler}>
          Add a Comment
        </button>
      )}
      {isAddingComment && <NewCommentForm onCommnetAdded={commentAddedHandler} />}
      {status === "completed" && loadedComments.length > 0 && (
        <CommentsList comments={loadedComments} />
      )}
    </section>
  );
};

export default Comments;
