import { useRef, useEffect } from "react";

import classes from "./NewCommentForm.module.css";
import useHttp from "../../hooks/use-http";
import { addComment } from "../../lib/app";
import { useParams, useHistory } from "react-router-dom";
import LoadingSpinner from "../UI/LoadingSpinner";

const NewCommentForm = (props) => {
  const commentTextRef = useRef();
  const { sendRequest, status } = useHttp(addComment);
  const param = useParams();
  const history = useHistory();

  const { quoteId } = param;

  const submitFormHandler = (event) => {
    event.preventDefault();

    const commentData = { text: commentTextRef.current.value };
    // optional: Could validate here

    // send comment to server
    sendRequest({ quoteId, commentData });
  };

  const { onCommnetAdded } = props;

  useEffect(() => {
    if (status === "completed") {
      history.push("/quotes/" + quoteId + "/comments");
      onCommnetAdded();
    }
  }, [status, history, quoteId, onCommnetAdded]);

  return (
    <form className={classes.form} onSubmit={submitFormHandler}>
      {status === "pending" && (
        <div className="centered">
          <LoadingSpinner />
        </div>
      )}
      <div className={classes.control} onSubmit={submitFormHandler}>
        <label htmlFor="comment">Your Comment</label>
        <textarea id="comment" rows="5" ref={commentTextRef}></textarea>
      </div>
      <div className={classes.actions}>
        <button className="btn">Add Comment</button>
      </div>
    </form>
  );
};

export default NewCommentForm;
