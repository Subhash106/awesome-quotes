import { useParams, Route, useHistory } from "react-router-dom";

import Comments from "../../components/comments/Comments";
import HighlightedQuote from "../quotes/HighlightedQuote";
import useHttp from "../../hooks/use-http";
import { getSingleQuote } from "../../lib/app";
import { useEffect } from "react";
import LoadingSpinner from "../UI/LoadingSpinner";
import NoQuotesFound from "../quotes/NoQuotesFound";

const QuoteDetails = () => {
  const param = useParams();
  const history = useHistory();

  const { sendRequest, data: loadedQuote, status, error } = useHttp(
    getSingleQuote,
    true
  );

  const { quoteId } = param;

  const showQuotesHandler = () => {
    history.push("/quotes/" + quoteId + "/comments");
  };

  useEffect(() => {
    sendRequest(quoteId);
  }, [sendRequest, quoteId]);

  if (status === "pending") {
    return (
      <div className="centered">
        <LoadingSpinner />
      </div>
    );
  }

  if (error) {
    return <div className="centered focused">{error}</div>;
  }

  if (status === "completed" && (!loadedQuote || !loadedQuote.text)) {
    return <NoQuotesFound />;
  }

  return (
    <section>
      <HighlightedQuote author={loadedQuote.author} text={loadedQuote.text} />
      <Route path={`/quotes/${param.quoteId}`} exact>
        <div style={{ textAlign: "center" }}>
          <button onClick={showQuotesHandler}>Show Comments</button>
        </div>
      </Route>
      <Route path="/quotes/:quoteId/comments">
        <Comments />
      </Route>
    </section>
  );
};

export default QuoteDetails;
