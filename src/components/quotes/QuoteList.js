import { Fragment, useEffect } from "react";
import { useDispatch } from "react-redux";
import { quoteActions } from "../../store/quote-slice";
import { useHistory, useLocation } from "react-router-dom";

import QuoteItem from "./QuoteItem";
import classes from "./QuoteList.module.css";
import LoadingSpinner from "../../components/UI/LoadingSpinner";

const QuoteList = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const queryParams = new URLSearchParams(location.search);

  const isSortingAscending = queryParams.get("sortBy") === "asc";

  console.log(isSortingAscending);

  useEffect(() => {
    if (isSortingAscending) {
      dispatch(quoteActions.sortAscending());
    } else {
      dispatch(quoteActions.sortDescending());
    }
  }, [dispatch, isSortingAscending]);

  const sortQuotesHandler = () => {
    history.push("/quotes?sortBy=" + (isSortingAscending ? "desc" : "asc"));
  };

  return (
    <Fragment>
      <div className={classes.sorting}>
        <button onClick={sortQuotesHandler}>
          Sort {isSortingAscending ? "Descending" : "Ascending"}
        </button>
      </div>
      <ul className={classes.list}>
        {props.quotes.map((quote) => (
          <QuoteItem
            key={quote.id}
            id={quote.id}
            author={quote.author}
            text={quote.text}
          />
        ))}
        {props.isLoading && (
          <div className={classes.loading}>
            <LoadingSpinner />
          </div>
        )}
      </ul>
    </Fragment>
  );
};

export default QuoteList;
