import { configureStore } from '@reduxjs/toolkit';
import quoteReduer from './quote-slice';


const store = configureStore({
    reducer: {
        quote: quoteReduer
    }
});

export default store;