import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [
    {
      id: "i1",
      text: "Test Quote",
      author: "Subhash Chandra",
    },
    {
      id: "i2",
      text: "Test Quote 2",
      author: "Chetan Bhagat",
    },
  ],
};

const quoteSlice = createSlice({
  name: "quote",
  initialState,
  reducers: {
    addQuote(state, action) {
      state.items.push({
        id: Math.random().toString(),
        author: action.payload.author,
        text: action.payload.text,
      });
    },
    sortAscending(state) {
        console.log('sorting ascending');
      //const itemsCopy = [...state.items];
      state.items.sort((a, b) => {
        return a.text - b.text ? 1 : -1;
      });

      //state.items = itemsCopy;
    },
    sortDescending(state) {
        console.log('sorting descending');
      // const itemsCopy = [...state.items];
      state.items.sort((a, b) => {
        return a.text - b.text ? -1 : 1;
      });

      // state.items = itemsCopy;
    },
  },
});

export const quoteActions = quoteSlice.actions;

export default quoteSlice.reducer;
